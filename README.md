# Moved to GitHub

This module migrated to https://github.com/atlassian-labs/aws-resources due to [JPERF-720](https://ecosystem.atlassian.net/browse/JPERF-720).
